# pages/urls.py

from django.urls import path, include

from .views import *
urlpatterns = [
    path('', HomeView.as_view(), name="home"),
    path('post/<int:pk>/', post_detail, name="post_detail")
]