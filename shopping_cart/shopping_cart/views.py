from django.shortcuts import render
from django.views.generic import TemplateView
#Class based views

class ContactView(TemplateView):
    template_name = 'contact.html'

    def get(self, requst, *args, **kwargs):
        return super().get(requst, *args, **kwargs)

class AboutView(TemplateView):
    template_name = 'about.html'

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

#def contact(request):
 #   return render(request, 'contact.html')

#def about(request):
 #   return render(request, 'about.html')