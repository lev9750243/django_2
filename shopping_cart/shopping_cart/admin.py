from django.contrib import admin
from .models import Posts, Category, Comment, FeaturedPost


admin.site.register(Posts)
admin.site.register(Category)
#admin.site.register(Author)
admin.site.register(Comment)
admin.site.register(FeaturedPost)